class SessionsController < ApplicationController
  def new
  end
  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to controller: "users", action: "show", id: user.id , :notice => "Welcome back, #{user.email}"
    else
      redirect_to root_path, :notice => "Invalid Username or password"
    end
  end

  def destroy
    reset_session
    flash[:info] = "Signed out successfully!"
    redirect_to root_path
  end
end
